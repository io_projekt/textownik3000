from Plik_Wejsciowy import * 
from Porownanie import *
from difflib import Differ
class PorownanieI():
    """Klasa reprezentująca klasę interfejsu PorownanieI"""
    def __init__(self):
        """
        Konstruktor klasy PorownanieI

            Parametry:
                czyPlikWyjsciowy(nie zdefiniowane): WIP
        """

        print("PorownanieI working")

    def porownajFragmenty(self,File1,File2):
        """
        Metoda porównująca fragmenty

            Parametry:
                pass
        """
        self.porownanie = Porownanie()
        self.fileToCompare1 = File1
        self.fileToCompare2 = File2
        print(self.fileToCompare1)
        self.index1 = self.index2 = 0
        differ = Differ()
        for line in differ.compare(self.fileToCompare1, self.fileToCompare2):
            if line[0] in ('-', '+'):
                if line[0] == '-':
                    print(f'Line {self.index1+1} from lines1: {line}')
                    self.porownanie.filesDiffBackLog.append(f'Line {self.index1+1} from file1: {line}')
                    self.index1 += 1
                    self.porownanie.fileDiff1.append(1)
                elif line[0] == '+':
                    print(f'Line {self.index2+1} from lines2: {line}')
                    self.porownanie.filesDiffBackLog.append(f'Line {self.index2+1} from file2: {line}')
                    self.index2 += 1
                    self.porownanie.fileDiff2.append(1)
            elif line[0] == '?':
                None
            else:
                self.index1 += 1
                self.index2 += 1
                self.porownanie.fileDiff1.append(0)
                self.porownanie.fileDiff2.append(0)
        return self.porownanie
        

    def dopasujFragmenty(self):
        """
        Metoda dopasowywująca fragmenty

            Parametry:
                pass
        """
        pass

    def utworzPlikWyjsciowy(self):
        """
        Metoda tworząca plik wyjściowy

            Parametry:
                pass
        """
        pass

    def zwrocAktualnyWynik(self):
        """
        Metoda zwracająca aktualny wynik

            Parametry:
                pass
        """
        pass   