﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using textownik3000;

namespace WinFormsApp1
{
    internal class Interfejs_Graficzny
    {
        Form1 f1;
        Form2 f2;
        public Interfejs_Graficzny() { }
        public void pokazOkno()
        {
            bool loadNextFiles = true;
            while (loadNextFiles == true)
            {
                f1 = new Form1();
                Application.Run(f1);
                if (f1.getCorrectFilesSelected())    //when user selected proper files
                {
                    f2 = new Form2(f1.getFilePath1(), f1.getFilePath2());
                    Application.Run(f2);
                    loadNextFiles = f2.loadNextFiles();
                }
                else //when user wants to close the window
                {
                    loadNextFiles = false;
                }
            }
        }
        public void przekazDane(int index1, int size1, int index2, int size2, bool difference1)
        {
            f2.addNewDifference(index1, index2, size1, size2, difference1);
        }
    }
}
