from Plik_Wejsciowy import * 
from Plik_Wyjsiowy import *
from Porownanie_I import *
from Porownanie import *

class Textownik():
    """
    Klasa kontrolera dla aplikacji textownik3000
    """
    def __init__(self):
        """
        Konstruktor klasy textownik

            Parametry:
                pass
        """
        self.filelines1 = []
        self.filelines2 = []
        self.fileDiff1 = []
        self.fileDiff2 = []
        self.filesDiffBackLog = []
        self.fileFirst = PlikWejsciowy()
        self.fileSecond = PlikWejsciowy()
        self.fileCompare = PorownanieI()

    def pobierzPliki(self,file1,file2):
        """
        Metoda służąca pobraniu plikiu

            Parametry:
                pass
        """   
        self.fileFirst.wczytajPlik(file1)
        self.fileSecond.wczytajPlik(file2)
        self.filelines1 = self.fileFirst.podzialPliku()
        self.filelines2 = self.fileSecond.podzialPliku()
        #with open(file1) as file_1, open(file2) as file_2:
        #    self.filelines1 = [line.rstrip() for line in file_1.readlines()]
        #    self.filelines2 = [line.rstrip() for line in file_2.readlines()]

    def uruchomPorownanie(self):
        """
        Metoda ponownie uruchamiajaca porównywane pliku

            Parametry:
                pass
        """ 
        self.compared = self.fileCompare.porownajFragmenty(self.filelines1,self.filelines2)
        self.fileDiff1 = self.compared.fileDiff1
        self.fileDiff2 = self.compared.fileDiff2
        self.filesDiffBackLog = self.compared.filesDiffBackLog


    def przerwijPorownanie(self):
        """
        Metoda przerywająca porównywanie plików

            Parametry:
                pass
        """
        pass

    def wyswietlAktualnyWynik(self):
        """
        Metoda wyświetlająca aktualny wynik porównania

            Parametry:
                pass
        """
        pass
