class Porownanie(object):
    """klasa reprezentująca Porownanie"""
    def __init__(self):
        """
        Konstruktor klasy Porownanie

            Parametry:
                pojemnik1(obiekt klasy FragmentPliku): 
                    pojemnik na pierwszy fragment porównania
                pojemnik2(obiekt klasy FragmentPliku): 
                    pojemnik na drugi fragment porównania
        """
        self.fileDiff1 = []
        self.fileDiff2 = []
        self.filesDiffBackLog = []