﻿namespace textownik3000
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        public Form2()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.richTextBox2 = new System.Windows.Forms.RichTextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.totalDifferencesLabel = new System.Windows.Forms.Label();
            this.currentDifferenceTextBox = new System.Windows.Forms.TextBox();
            this.previousDifferenceButton = new System.Windows.Forms.Button();
            this.nextDifferenceButton = new System.Windows.Forms.Button();
            this.file1TextButton = new System.Windows.Forms.Button();
            this.file2TextButton = new System.Windows.Forms.Button();
            this.mergeButton = new System.Windows.Forms.Button();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.SuspendLayout();
            // 
            // richTextBox1
            // 
            this.richTextBox1.CausesValidation = false;
            this.richTextBox1.Location = new System.Drawing.Point(80, 9);
            this.richTextBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.Size = new System.Drawing.Size(440, 480);
            this.richTextBox1.TabIndex = 0;
            this.richTextBox1.Text = "";
            this.richTextBox1.TextChanged += new System.EventHandler(this.richTextBox1_TextChanged);
            // 
            // richTextBox2
            // 
            this.richTextBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox2.CausesValidation = false;
            this.richTextBox2.Location = new System.Drawing.Point(640, 9);
            this.richTextBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.richTextBox2.Name = "richTextBox2";
            this.richTextBox2.ReadOnly = true;
            this.richTextBox2.Size = new System.Drawing.Size(440, 480);
            this.richTextBox2.TabIndex = 1;
            this.richTextBox2.Text = "";
            this.richTextBox2.TextChanged += new System.EventHandler(this.richTextBox2_TextChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(3, 9);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(53, 69);
            this.button1.TabIndex = 2;
            this.button1.Text = "Select files again";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.buttonReturn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(1135, 66);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(12, 15);
            this.label1.TabIndex = 3;
            this.label1.Text = "/";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // totalDifferencesLabel
            // 
            this.totalDifferencesLabel.AutoSize = true;
            this.totalDifferencesLabel.Location = new System.Drawing.Point(1163, 66);
            this.totalDifferencesLabel.Name = "totalDifferencesLabel";
            this.totalDifferencesLabel.Size = new System.Drawing.Size(13, 15);
            this.totalDifferencesLabel.TabIndex = 4;
            this.totalDifferencesLabel.Text = "1";
            this.totalDifferencesLabel.Click += new System.EventHandler(this.totalDifferencesLabel_Click);
            // 
            // currentDifferenceTextBox
            // 
            this.currentDifferenceTextBox.Location = new System.Drawing.Point(1086, 63);
            this.currentDifferenceTextBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.currentDifferenceTextBox.Name = "currentDifferenceTextBox";
            this.currentDifferenceTextBox.Size = new System.Drawing.Size(43, 23);
            this.currentDifferenceTextBox.TabIndex = 5;
            this.currentDifferenceTextBox.Text = "1";
            this.currentDifferenceTextBox.TextChanged += new System.EventHandler(this.currentDifferenceTextBox_TextChanged);
            // 
            // previousDifferenceButton
            // 
            this.previousDifferenceButton.Location = new System.Drawing.Point(1110, 31);
            this.previousDifferenceButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.previousDifferenceButton.Name = "previousDifferenceButton";
            this.previousDifferenceButton.Size = new System.Drawing.Size(52, 22);
            this.previousDifferenceButton.TabIndex = 6;
            this.previousDifferenceButton.Text = "-";
            this.previousDifferenceButton.UseVisualStyleBackColor = true;
            this.previousDifferenceButton.Click += new System.EventHandler(this.previousDifferenceButton_Click);
            // 
            // nextDifferenceButton
            // 
            this.nextDifferenceButton.Location = new System.Drawing.Point(1110, 96);
            this.nextDifferenceButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.nextDifferenceButton.Name = "nextDifferenceButton";
            this.nextDifferenceButton.Size = new System.Drawing.Size(52, 22);
            this.nextDifferenceButton.TabIndex = 7;
            this.nextDifferenceButton.Text = "+";
            this.nextDifferenceButton.UseVisualStyleBackColor = true;
            this.nextDifferenceButton.Click += new System.EventHandler(this.nextDifferenceButton_Click);
            // 
            // file1TextButton
            // 
            this.file1TextButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.file1TextButton.AutoSize = true;
            this.file1TextButton.Location = new System.Drawing.Point(80, 507);
            this.file1TextButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.file1TextButton.Name = "file1TextButton";
            this.file1TextButton.Size = new System.Drawing.Size(156, 25);
            this.file1TextButton.TabIndex = 8;
            this.file1TextButton.Text = "Select text from file 1";
            this.file1TextButton.UseVisualStyleBackColor = true;
            this.file1TextButton.Click += new System.EventHandler(this.file1TextButton_Click);
            // 
            // file2TextButton
            // 
            this.file2TextButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.file2TextButton.AutoSize = true;
            this.file2TextButton.Location = new System.Drawing.Point(926, 505);
            this.file2TextButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.file2TextButton.Name = "file2TextButton";
            this.file2TextButton.Size = new System.Drawing.Size(154, 27);
            this.file2TextButton.TabIndex = 9;
            this.file2TextButton.Text = "Select text from file 2";
            this.file2TextButton.UseVisualStyleBackColor = true;
            this.file2TextButton.Click += new System.EventHandler(this.file2TextButton_Click);
            // 
            // mergeButton
            // 
            this.mergeButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mergeButton.Location = new System.Drawing.Point(525, 507);
            this.mergeButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.mergeButton.Name = "mergeButton";
            this.mergeButton.Size = new System.Drawing.Size(108, 25);
            this.mergeButton.TabIndex = 10;
            this.mergeButton.Text = "Merge";
            this.mergeButton.UseVisualStyleBackColor = true;
            this.mergeButton.Click += new System.EventHandler(this.mergeButton_Click);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.saveFileDialog1_FileOk);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(30)))), ((int)(((byte)(60)))));
            this.ClientSize = new System.Drawing.Size(1184, 561);
            this.Controls.Add(this.mergeButton);
            this.Controls.Add(this.file2TextButton);
            this.Controls.Add(this.file1TextButton);
            this.Controls.Add(this.nextDifferenceButton);
            this.Controls.Add(this.previousDifferenceButton);
            this.Controls.Add(this.currentDifferenceTextBox);
            this.Controls.Add(this.totalDifferencesLabel);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.richTextBox2);
            this.Controls.Add(this.richTextBox1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MaximizeBox = false;
            this.Name = "Form2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Textownik3000";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private RichTextBox richTextBox1;
        private RichTextBox richTextBox2;
        private Button button1;
        private Label label1;
        private Label totalDifferencesLabel;
        private TextBox currentDifferenceTextBox;
        private Button previousDifferenceButton;
        private Button nextDifferenceButton;
        private Button file1TextButton;
        private Button file2TextButton;
        private Button mergeButton;
        private SaveFileDialog saveFileDialog1;
    }
}