using WinFormsApp1;

namespace textownik3000
{
    internal static class Program
    {
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            // To customize application configuration such as set high DPI settings or default font,
            // see https://aka.ms/applicationconfiguration.
            ApplicationConfiguration.Initialize();
            Interfejs_Graficzny gui = new Interfejs_Graficzny();
            gui.pokazOkno();
            gui.przekazDane(2, 5, 1, 2, false);
        }
    }
}