﻿using Fasterflect;
using Microsoft.Scripting.Hosting;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace textownik3000
{
    public partial class Form2 : Form
    {
        string path1;
        string path2;
        string mergePath;
        int totalDifferences;
        int currentDifference;
        bool returnToMainForm;
        List<Difference> differences;
        List<string> textFileList1 = new List<string>();
        List<string> textFileList2 = new List<string>();
        List<int> textFileDiffList1 = new List<int>();
        List<int> textFileDiffList2 = new List<int>();
        List<string> linesDiffBackLogList = new List <string>();
        public Form2(string f1, string f2)
        {
            path1 = f1;
            path2 = f2;
            initiatePythonCompare();
            differences = new List<Difference>();
            totalDifferences = 0;
            currentDifference = 0;
            addDifferences(textFileList1, textFileList2, textFileDiffList1, textFileDiffList2);
            returnToMainForm = false;
            mergePath = string.Empty;
            InitializeComponent();
            totalDifferencesLabel.Text = totalDifferences.ToString();
            currentDifferenceTextBox.Text = currentDifference.ToString();
            loadFiles(textFileList1, textFileList2);
            highlightDifferences();
        }

        private void buttonReturn_Click(object sender, EventArgs e)
        {
            returnToMainForm = true;
            this.Close();
        }

        private void previousDifferenceButton_Click(object sender, EventArgs e)
        {
            totalDifferencesLabel.Text = totalDifferences.ToString();
            if (currentDifference > 1)
            {
                setToDefault(currentDifference - 1);
                --currentDifference;
                currentDifferenceTextBox.Text = currentDifference.ToString();
                highlightString(currentDifference - 1);
            }
            else
            {
                MessageBox.Show("Current difference has to be over 0.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void nextDifferenceButton_Click(object sender, EventArgs e)
        {
            totalDifferencesLabel.Text = totalDifferences.ToString();
            if (currentDifference < totalDifferences)
            {
                setToDefault(currentDifference - 1);
                ++currentDifference;
                currentDifferenceTextBox.Text = currentDifference.ToString();
                highlightString(currentDifference - 1);
            }
            else
            {
                MessageBox.Show("Current difference cannot be greater than total number of differences.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void file1TextButton_Click(object sender, EventArgs e)
        {
            if (currentDifference - 1 < differences.Count())
            {
                differences.ElementAt(currentDifference - 1).setDifference1(true);
                highlightString(currentDifference - 1);
            }
        }

        private void file2TextButton_Click(object sender, EventArgs e)
        {
            if (currentDifference - 1 < differences.Count())
            {
                differences.ElementAt(currentDifference - 1).setDifference1(false);
                highlightString(currentDifference - 1);
            }
        }

        private void setToDefault(int index)
        {
            try
            {
                if (differences.ElementAt(currentDifference - 1) != null)
                {
                    richTextBox1.SelectionStart = (int)differences.ElementAt(index).getIndex1();
                    richTextBox1.SelectionLength = (int)differences.ElementAt(index).getSize1();
                    richTextBox1.SelectionBackColor = Color.IndianRed;
                    richTextBox2.SelectionStart = (int)differences.ElementAt(index).getIndex2();
                    richTextBox2.SelectionLength = (int)differences.ElementAt(index).getSize2();
                    richTextBox2.SelectionBackColor = Color.IndianRed;
                }
            }
            catch (ArgumentOutOfRangeException)
            {

            }
        }
        private void highlightString(int index)
        {
            try
            {
                if (differences.ElementAt(currentDifference - 1) != null)
                {
                    richTextBox1.SelectionStart = (int)differences.ElementAt(index).getIndex1();
                    richTextBox1.SelectionLength = (int)differences.ElementAt(index).getSize1();
                    richTextBox1.SelectionBackColor = differences.ElementAt(index).getDifference1() == true ? Color.DeepSkyBlue : Color.Gold;
                    richTextBox2.SelectionStart = (int)differences.ElementAt(index).getIndex2();
                    richTextBox2.SelectionLength = (int)differences.ElementAt(index).getSize2();
                    richTextBox2.SelectionBackColor = differences.ElementAt(index).getDifference1() == false ? Color.DeepSkyBlue : Color.Gold;
                    richTextBox1.ScrollToCaret();   // scrolls to highlighted text
                    richTextBox2.ScrollToCaret();   // scrolls to highlighted text
                }
            }
            catch (ArgumentOutOfRangeException)
            {

            }
        }
        public bool loadNextFiles()
        {
            return returnToMainForm;
        }

        public void addNewDifference(int index1, int size1, int index2, int size2, bool difference1)
        {
            differences.Add(new Difference(index1, size1, index2, size2, difference1));
            ++totalDifferences;
        }

        private void mergeButton_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                mergePath = saveFileDialog1.FileName;
                if (Path.GetExtension(mergePath) != ".txt")
                {
                    mergePath = String.Empty;
                    MessageBox.Show("Path or file extension incorrect.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    using var sw = new StreamWriter(mergePath);
                    foreach (var item in textFileList1)
                    {
                        sw.WriteLine(item);
                    }
                    
                }
            }
        }
        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            saveFileDialog1.InitialDirectory = Environment.CurrentDirectory;
            saveFileDialog1.Filter = "txt files (*.txt)|*.txt";
        }
        public string getMergePath()
        {
            return mergePath;
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }
        private void addDifferences(List<string> textFileList1, List<string> textFileList2, List<int> textFileDiffList1, List<int> textFileDiffList2)
        {
            int index1 = 0;
            int index2 = 0;
            int max = Math.Max(textFileDiffList1.Count, textFileDiffList2.Count);
            for (int i = 0; i < max; ++i)
            {
                int size1, size2;
                if (i < textFileDiffList1.Count && textFileDiffList1[i] == 1 || i < textFileDiffList2.Count && textFileDiffList2[i] == 1)
                {
                    if (i < textFileDiffList1.Count && textFileDiffList1[i] == 1)
                    {
                        size1 = i < textFileList1.Count ? textFileList1[i].Length + 1 : 0;
                    }
                    else
                    {
                        size1 = 0;
                    }
                    if (i < textFileDiffList2.Count && textFileDiffList2[i] == 1)
                    {
                        size2 = i < textFileList2.Count ? textFileList2[i].Length + 1 : 0;
                    }
                    else
                    {
                        size2 = 0;
                    }
                    addNewDifference(index1, size1, index2, size2, true);
                }
                if (i < textFileList1.Count)
                    index1 += textFileList1[i].Length + 1;
                if (i < textFileList2.Count)
                    index2 += textFileList2[i].Length + 1;
            }
        }

        private void highlightDifferences()
        {
            for (int i = 0; i < differences.Count; ++i)
            {
                if (differences.ElementAt(i) != null)
                {
                    richTextBox1.SelectionStart = (int)differences.ElementAt(i).getIndex1();
                    richTextBox1.SelectionLength = (int)differences.ElementAt(i).getSize1();
                    richTextBox1.SelectionBackColor = Color.IndianRed;
                    richTextBox2.SelectionStart = (int)differences.ElementAt(i).getIndex2();
                    richTextBox2.SelectionLength = (int)differences.ElementAt(i).getSize2();
                    richTextBox2.SelectionBackColor = Color.IndianRed;
                }
            }
        }
        private void loadFiles(List<string> textFileList1, List<string> textFileList2)
        {
            for (int i = 0; i < textFileList1.Count; i++)
            {
                richTextBox1.AppendText(textFileList1.ElementAt(i) + "\n");
            }
            for (int i = 0; i < textFileList2.Count; i++)
            {
                richTextBox2.AppendText(textFileList2.ElementAt(i) + "\n");
            }
        }

        private void richTextBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void initiatePythonCompare()
        {
            var engine = IronPython.Hosting.Python.CreateEngine();
            var scope = engine.CreateScope();
            ScriptSource source = engine.CreateScriptSourceFromFile("textownik.py");
            var compilation = source.Compile();
            var result = compilation.Execute(scope);
            dynamic Textownik = scope.GetVariable("Textownik");
            dynamic textownik = Textownik();
            textownik.pobierzPliki(path1, path2);
            textownik.uruchomPorownanie();
            dynamic textFile1 = textownik.filelines1;
            dynamic textFile2 = textownik.filelines2;
            dynamic textFileDiff1 = textownik.fileDiff1;
            dynamic textFileDiff2 = textownik.fileDiff2;
            dynamic linesDiffBackLog = textownik.filesDiffBackLog;
            foreach (var item in textFile1)
            {
                textFileList1.Add((string)item);
            }
            foreach (var item in textFile2)
            {
                textFileList2.Add((string)item);
            }
            foreach (var item in textFileDiff1)
            {
                textFileDiffList1.Add(item);
            }
            foreach (var item in textFileDiff2)
            {
                textFileDiffList2.Add(item);
            }
            foreach (var item in linesDiffBackLog)
            {
                linesDiffBackLogList.Add((string)item);
            }
        }

        private void Form2_Load(object sender, EventArgs e)
        {

        }

        private void currentDifferenceTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void totalDifferencesLabel_Click(object sender, EventArgs e)
        {

        }
    }
}