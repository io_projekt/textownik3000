class PlikWejsciowy():
    """Klasa reprezentująca PlikWejsciowy"""
    filelines = []
    filepath = ""
    def __init__(self):
        """
        Konstruktor klasy PlikWejsciowy
        """
        self.filelines = []
        self.filepath = ""

    def wczytajPlik(self,path):
        """
        Metoda wczytująca plik

            Parametry:
                pass
        """
        self.filepath = path
        
    
    def podzialPliku(self):
        """
        Metoda tworząca podział pliku

            Parametry:
                pass
        """
        with open(self.filepath) as file_1:
            self.filelines = [line.rstrip() for line in file_1.readlines()]
        return self.filelines
