import unittest
from Plik_Wejsciowy import * 
from textownik import *
from Porownanie_I import *
from Porownanie import *

class testTextownik(unittest.TestCase):
    """Tests for backEnd of Textownik3000"""
    
    def test_pobierzPliki(self):
        test_Textownik = Textownik()
        test_Textownik.pobierzPliki("plik.txt","plik1.txt")
        self.assertEqual(test_Textownik.filelines1, ['plik 1', 'tu nowa linia', 'tu numer 1'])
        self.assertEqual(test_Textownik.filelines2, ['plik 2', 'tu numer 1'])

    def test_uruchomPorownanie(self):
        test_Textownik = Textownik()
        test_Textownik.filelines1 = ['plik 1', 'tu nowa linia', 'tu numer 1']
        test_Textownik.filelines2 = ['plik 2', 'tu numer 1']
        test_Textownik.uruchomPorownanie()
        self.assertEqual(test_Textownik.fileDiff1,[1, 1, 0])
        self.assertEqual(test_Textownik.fileDiff2,[1, 0])
        self.assertEqual(test_Textownik.filesDiffBackLog,['Line 1 from file1: - plik 1', 'Line 1 from file2: + plik 2', 'Line 2 from file1: - tu nowa linia'])

if __name__ == '__main__':
    unittest.main()